# Content Close

This module will let the admin set the expiry time for Content Types.
After setting expiry time, if the expiry time has passed, the user will not be
able to create a new node for that content type.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/content_close).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/content_close).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to _Administration » Configuration » Web Services » Content Close_ configure settings to
   configure expiration dates.
2. From the Content Type List set the expiry dates and times. Save configuration.


## Maintainers

- Mahtab Alam - [mahtab_alam](https://www.drupal.org/u/mahtab_alam)
- Mahaveer Singh Panwar - [mahaveer003](https://www.drupal.org/u/mahaveer003)
- Nishant kumar - [nishantkumar155](https://www.drupal.org/u/nishantkumar155)
